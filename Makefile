IMAGE_NAME=mlops-project
REGISTRY=registry.gitlab.com/azabirnik1/mlops
CI_DOCKERFILE=Dockerfile.ci

.PHONY: build push run build-ci push-ci

build:
	@echo "Building the Docker image..."
	docker build -t $(IMAGE_NAME) .

push:
	@echo "Pushing the CI Docker image to GitLab Registry..."
	docker push $(REGISTRY)/$(IMAGE_NAME)

run:
	@echo "Running the Docker container..."
	docker run --rm -d -p 8898:8898 $(IMAGE_NAME)

build-ci:
	@echo "Building the CI Docker image..."
	docker build -f $(CI_DOCKERFILE) -t $(REGISTRY)/$(IMAGE_NAME)-ci .

push-ci:
	@echo "Pushing the CI Docker image to GitLab Registry..."
	docker push $(REGISTRY)/$(IMAGE_NAME)-ci
