# MLOps Project

Welcome to the MLOps Project repository, where we focus on applying best practices for Machine Learning Operations (MLOps) and production in data science research.

## Overview

This repository is part of the course on MLOps and Production in DS Research 3.0 and serves as the foundation for implementing and managing machine learning models effectively and efficiently.

## Getting Started

To get started with this project, follow these steps:

1. **Clone the repository**: `git clone https://gitlab.com/username/mlops.git`
2. **Install dependencies**: Run `pip install -r requirements.txt` to install the necessary dependencies.
3. **Set up pre-commit hooks**: Ensure you have `pre-commit` installed by running `pip install pre-commit`, then execute `pre-commit install` to activate the hooks.

## Using the Repository

When using this repository, please adhere to the following workflow:

1. **Create a feature branch**: For any new changes, create a branch from `main` using the naming convention `feature/your_feature_name`.
2. **Commit your changes**: Make sure your commits are clear and understandable.
3. **Open a pull request**: Once your feature is ready, open a pull request against `main` for review.

## Building and Running the Docker Container

To simplify the development and execution environment setup, this project utilizes Docker. Here's how to build and run the project using Docker:

### Building the Docker Image:
To build the Docker image, run the following command:
```
make build
```

This command will build a Docker image named mlops-project using the Dockerfile located in the root directory of the project.

### Running the Docker Container:
After building the image, you can run a container with the following command:
```
make run
```

This command will start a Jupyter Notebook server accessible at http://localhost:8888.

## Contributing

We welcome contributions! Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to submit pull requests, the process for submitting bugs, and the general workflow.

## Repository Structure

Below is an outline of the key files and folders within this repository:

- `README.md`: Provides an overview and documentation for the project.
- `CONTRIBUTING.md`: Guidelines for contributing to the project.
- `LICENSE`: The license file that describes the permissions and limitations for the code use.
- `requirements.txt`: Specifies the Python packages required for the project.
- `pyproject.toml`: Configuration settings for `black` and `isort` to ensure consistent code formatting.
- `.pre-commit-config.yaml`: Configuration for pre-commit hooks that run checks before each commit.

## Issues

If you encounter any issues or have questions, please file an issue in the GitLab repository.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

- Course instructors and contributors who provided insights and feedback.
- All contributors who have helped to shape this project.

Thank you for visiting the MLOps Project repository!
