FROM mambaorg/micromamba:latest

WORKDIR /app

RUN micromamba install -y -p /opt/conda python=3.10 pip -c conda-forge && \
    micromamba install -y -p /opt/conda pipenv -c conda-forge && \
    micromamba clean --all

COPY Pipfile Pipfile.lock /app/

RUN /opt/conda/bin/pipenv install --deploy --ignore-pipfile

COPY . /app/

EXPOSE 8898

CMD ["pipenv", "run", "jupyter", "notebook", "--ip=0.0.0.0", "--allow-root", "--NotebookApp.token=''", "--NotebookApp.password=''", "--no-browser", "--port=8888"]
